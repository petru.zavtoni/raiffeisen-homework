package com.raiffesein.petru;

import com.raiffesein.petru.api.controllers.AccountsController;
import com.raiffesein.petru.api.models.AccountDTO;
import com.raiffesein.petru.persistence.entities.AccountEntity;
import com.raiffesein.petru.persistence.repos.AccountRepo;
import com.raiffesein.petru.services.exceptions.ItemNotFoundException;
import com.raiffesein.petru.ws.ExchangeWebService;
import com.raiffesein.petru.ws.dto.ExchangeRatesResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class HomeworkApplicationTests {

	public static final String IBAN_1 = "IBAN1";
	public static final String IBAN_2 = "IBAN2";
	public static final long ACC_1_ID = 1L;
	public static final long ACC_2_ID = 2L;
	public static final String RATES_BASE_CURRENCY = "EUR";
	public static final String RON_CURRENCY = "RON";
	public static final String USD_CURRENCY = "USD";

	@Autowired
	private AccountsController controller;

	@Autowired
	private AccountRepo repository;

	@MockBean
	private ExchangeWebService ws;

	@BeforeAll
	public void init() {
		initAccountsData();
		initWebServiceData();
	}

	@Test
	void getAccountBalanceAfterExchange() {
		AccountDTO result = controller.getBalance(ACC_1_ID);

		assertEquals(IBAN_1, result.getIban());
		assertEquals(RATES_BASE_CURRENCY, result.getCurrency());
		assertTrue(result.getBalance().compareTo(BigDecimal.TEN) < 0);

		verify(ws, times(1)).getExchangeRates();
	}

	@Test
	void getAccountBalanceAfterExchange_NotFound() {
		assertThrows(ItemNotFoundException.class, () -> controller.getBalance(7L));
	}

	private void initAccountsData() {
		AccountEntity acc1 = new AccountEntity();
		acc1.setId(ACC_1_ID);
		acc1.setIban(IBAN_1);
		acc1.setCurrency(Currency.getInstance("USD"));
		acc1.setBalance(BigDecimal.TEN);
		acc1.setCreatedAt(LocalDateTime.now());
		acc1.setLastUpdated(LocalDateTime.now());

		AccountEntity acc2 = new AccountEntity();
		acc2.setId(ACC_2_ID);
		acc2.setIban(IBAN_2);
		acc2.setCurrency(Currency.getInstance("RON"));
		acc2.setBalance(BigDecimal.TEN);
		acc2.setCreatedAt(LocalDateTime.now());
		acc2.setLastUpdated(LocalDateTime.now());

		repository.save(acc1);
		repository.save(acc2);
	}

	private void initWebServiceData() {
		ExchangeRatesResponse response = new ExchangeRatesResponse();
		response.setBase(RATES_BASE_CURRENCY);
		response.setRates(Map.of(
			RON_CURRENCY, BigDecimal.valueOf(4.5),
			USD_CURRENCY, BigDecimal.valueOf(1.1)
		));

		Mockito.when(ws.getExchangeRates()).thenReturn(response);
	}

}
