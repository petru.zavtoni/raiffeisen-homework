package com.raiffesein.petru.services.impl;

import com.raiffesein.petru.persistence.entities.AccountEntity;
import com.raiffesein.petru.persistence.repos.AccountRepo;
import com.raiffesein.petru.services.exceptions.ItemNotFoundException;
import com.raiffesein.petru.services.models.AccountData;
import com.raiffesein.petru.ws.ExchangeWebService;
import com.raiffesein.petru.ws.dto.ExchangeRatesResponse;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountServiceImplTest {

    public static final Long ACCOUNT_ID = 1L;
    public static final String ACCOUNT_CURRENCY = "USD";
    public static final String RATES_BASE_CURRENCY = "EUR";
    public static final BigDecimal ACCOUNT_BALANCE = BigDecimal.valueOf(100);
    public static final String ACCOUNT_IBAN = "IBAN";

    AccountRepo accountRepo = mock(AccountRepo.class);
    ExchangeWebService exchangeWebService = mock(ExchangeWebService.class);

    private final AccountServiceImpl service = new AccountServiceImpl(accountRepo, exchangeWebService);

    @Test
    public void getBalance_AccountNotFound() {
        when(accountRepo.findById(ACCOUNT_ID)).thenReturn(Optional.empty());

        assertThrows(
            ItemNotFoundException.class,
            () -> service.getBalance(ACCOUNT_ID)
        );
    }

    @Test
    public void getBalance() {
        AccountEntity account = new AccountEntity();
        account.setIban(ACCOUNT_IBAN);
        account.setBalance(ACCOUNT_BALANCE);
        account.setCurrency(Currency.getInstance(ACCOUNT_CURRENCY));

        ExchangeRatesResponse rates = new ExchangeRatesResponse();
        rates.setBase(RATES_BASE_CURRENCY);
        rates.setRates(Map.of(ACCOUNT_CURRENCY, BigDecimal.valueOf(1.1)));

        when(accountRepo.findById(ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(exchangeWebService.getExchangeRates()).thenReturn(rates);

        AccountData result = service.getBalance(ACCOUNT_ID);

        assertEquals(ACCOUNT_IBAN, result.getIban());
        assertEquals(RATES_BASE_CURRENCY, result.getCurrency().getCurrencyCode());
        assertTrue(result.getBalance().compareTo(ACCOUNT_BALANCE) < 0);
    }

    @Test
    public void getBalance_resultMapping() {
        AccountEntity account = new AccountEntity();
        account.setIban(ACCOUNT_IBAN);
        account.setBalance(ACCOUNT_BALANCE);
        account.setCurrency(Currency.getInstance(ACCOUNT_CURRENCY));
        account.setCreatedAt(LocalDateTime.MIN);
        account.setLastUpdated(LocalDateTime.MAX);
        account.setId(ACCOUNT_ID);

        ExchangeRatesResponse rates = new ExchangeRatesResponse();
        rates.setBase(RATES_BASE_CURRENCY);
        rates.setRates(Map.of(ACCOUNT_CURRENCY, BigDecimal.valueOf(1.1)));

        when(accountRepo.findById(ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(exchangeWebService.getExchangeRates()).thenReturn(rates);

        AccountData result = service.getBalance(ACCOUNT_ID);

        assertEquals(ACCOUNT_IBAN, result.getIban());
        assertEquals(LocalDateTime.MAX, result.getLastUpdated());
        assertEquals(RATES_BASE_CURRENCY, result.getCurrency().getCurrencyCode());
        assertTrue(result.getBalance().compareTo(ACCOUNT_BALANCE) < 0);
    }

}
