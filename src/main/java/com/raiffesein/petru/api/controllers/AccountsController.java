package com.raiffesein.petru.api.controllers;

import com.raiffesein.petru.api.models.AccountDTO;
import com.raiffesein.petru.services.AccountService;
import com.raiffesein.petru.services.models.AccountData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.RoundingMode;

@RestController
public class AccountsController {

    private final AccountService accountService;

    @Autowired
    public AccountsController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(value = "/accounts/{id}", produces = "application/json")
    public AccountDTO getBalance(@PathVariable Long id) {
        AccountData account = accountService.getBalance(id);

        return buildResponse(account);
    }

    private AccountDTO buildResponse(AccountData account) {
        return AccountDTO.builder()
            .iban(account.getIban())
            .currency(account.getCurrency().getCurrencyCode())
            .balance(account.getBalance().setScale(2, RoundingMode.CEILING))
            .build();
    }
}
