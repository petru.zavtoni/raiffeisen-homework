package com.raiffesein.petru.api.models;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class AccountDTO {
    String iban;

    String currency;

    BigDecimal balance;
}
