package com.raiffesein.petru.persistence.repos;

import com.raiffesein.petru.persistence.entities.AccountEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepo extends CrudRepository<AccountEntity, Long> {
}
