package com.raiffesein.petru.persistence.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;

@Data
@Entity(name = "accounts")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String iban;

    private Currency currency;

    private BigDecimal balance;

    private LocalDateTime createdAt;

    private LocalDateTime lastUpdated;
}
