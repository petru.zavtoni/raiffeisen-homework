package com.raiffesein.petru.ws;

import com.raiffesein.petru.services.exceptions.DependencyException;
import com.raiffesein.petru.ws.dto.ExchangeRatesResponse;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

import static com.raiffesein.petru.config.CachingConfig.EXCHANGE_RATES;

@Service
public class ExchangeWebService {
    private final static String API_VERSION = "v1";
    private final static String PROPERTY_KEY = "access_key";
    private final static String PROPERTY_VALUE = "97ed016d8fb7e57bfb3f27a92dd7d2b1";
    public static final String CIRCUIT_BREAKER = "circuitbreaker";
    public static final String CACHED_ER = "cachedER";

    private final WebClient webClient;
    private final CircuitBreakerRegistry circuitBreakerRegistry;
    private final CacheManager cacheManager;

    @Autowired
    public ExchangeWebService(
        WebClient webClient,
        CircuitBreakerRegistry circuitBreakerRegistry,
        CacheManager cacheManager
    ) {
        this.webClient = webClient;
        this.circuitBreakerRegistry = circuitBreakerRegistry;
        this.cacheManager = cacheManager;
    }

    @Cacheable(value = EXCHANGE_RATES, key = "#root.target.CACHED_ER")
    @Retry(name = "exchangeRates", fallbackMethod = "fallback")
    public ExchangeRatesResponse getExchangeRates() {
        CircuitBreaker circuitBreaker = circuitBreakerRegistry.circuitBreaker(CIRCUIT_BREAKER);

        UriComponents uri = UriComponentsBuilder.newInstance()
           .scheme("http")
           .host("api.exchangeratesapi.io")
           .path(String.format("/%s/latest", API_VERSION))
           .queryParam(PROPERTY_KEY, PROPERTY_VALUE)
           .build();

        return webClient.get()
            .uri(uri.toUri())
            .retrieve()
            .bodyToMono(ExchangeRatesResponse.class)
            .transformDeferred(CircuitBreakerOperator.of(circuitBreaker))
            .block();
    }

    private ExchangeRatesResponse fallback(Exception ex) {
        Optional<ExchangeRatesResponse> cached = Optional.ofNullable(
            cacheManager.getCache(EXCHANGE_RATES).get(CACHED_ER, ExchangeRatesResponse.class)
        );

        if (cached.isPresent()) {
            return cached.get();
        } else {
            throw new DependencyException(ExchangeWebService.class.getSimpleName());
        }
    }
}
