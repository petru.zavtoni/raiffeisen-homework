package com.raiffesein.petru.ws.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

@Data
public class ExchangeRatesResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    Map<String, BigDecimal> rates;
    String base;
}
