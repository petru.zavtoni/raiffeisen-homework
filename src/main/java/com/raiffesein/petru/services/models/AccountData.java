package com.raiffesein.petru.services.models;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;

@Data
@Builder
public class AccountData {
    String iban;

    Currency currency;

    BigDecimal balance;

    LocalDateTime lastUpdated;
}
