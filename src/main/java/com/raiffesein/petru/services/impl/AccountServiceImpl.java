package com.raiffesein.petru.services.impl;

import com.raiffesein.petru.persistence.entities.AccountEntity;
import com.raiffesein.petru.persistence.repos.AccountRepo;
import com.raiffesein.petru.services.AccountService;
import com.raiffesein.petru.services.exceptions.ItemNotFoundException;
import com.raiffesein.petru.services.models.AccountData;
import com.raiffesein.petru.ws.ExchangeWebService;
import com.raiffesein.petru.ws.dto.ExchangeRatesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepo accountRepo;
    private final ExchangeWebService exchangeWebService;

    @Autowired
    public AccountServiceImpl(AccountRepo accountRepo, ExchangeWebService exchangeWebService) {
        this.accountRepo = accountRepo;
        this.exchangeWebService = exchangeWebService;
    }

    @Override
    public AccountData getBalance(Long accountId) {
        Optional<AccountEntity> account = accountRepo.findById(accountId);

        if (account.isPresent()) {
            ExchangeRatesResponse exchangeRate = exchangeWebService.getExchangeRates();

            AccountEntity accountData = account.get();

            return buildAccountData(accountData, exchangeRate);
        } else {
            throw new ItemNotFoundException(
                AccountEntity.class.getSimpleName(),
                String.format("Id=%d", accountId)
            );
        }
    }

    private AccountData buildAccountData(
        AccountEntity accountEntity,
        ExchangeRatesResponse exchangeRates
    ) {
        BigDecimal exchangeRate = exchangeRates.getRates()
            .get(accountEntity.getCurrency().getCurrencyCode());

        return AccountData.builder()
            .iban(accountEntity.getIban())
            .currency(Currency.getInstance(exchangeRates.getBase()))
            .balance(accountEntity.getBalance().divide(exchangeRate, 5, RoundingMode.HALF_UP))
            .lastUpdated(accountEntity.getLastUpdated())
            .build();
    }
}
