package com.raiffesein.petru.services;

import com.raiffesein.petru.services.models.AccountData;

public interface AccountService {
    AccountData getBalance(Long accountId);
}
