package com.raiffesein.petru.services.exceptions;

import lombok.Getter;

@Getter
public class DependencyException extends RuntimeException {

    private final String service;

    public DependencyException(String service) {
        super(String.format("%s service has failed", service));

        this.service = service;
    }
}
