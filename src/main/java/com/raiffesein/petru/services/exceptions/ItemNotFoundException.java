package com.raiffesein.petru.services.exceptions;

import lombok.Getter;

@Getter
public class ItemNotFoundException extends RuntimeException {

    private final String itemName;

    public ItemNotFoundException(String itemName, String searchCriteria) {
        super(String.format("%s Entity was not found by %s", itemName, searchCriteria));

        this.itemName = itemName;
    }
}
