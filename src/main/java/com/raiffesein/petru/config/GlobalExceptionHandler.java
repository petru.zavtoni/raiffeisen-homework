package com.raiffesein.petru.config;

import com.raiffesein.petru.services.exceptions.DependencyException;
import com.raiffesein.petru.services.exceptions.ErrorMessage;
import com.raiffesein.petru.services.exceptions.ItemNotFoundException;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ItemNotFoundException.class)
    protected ResponseEntity<Object> handleConflict(ItemNotFoundException ex) {

        logger.warn(ex.getMessage());

        ErrorMessage error = new ErrorMessage(String.format("%s was not found", ex.getItemName()));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(CallNotPermittedException.class)
    public ResponseEntity<Object> handleCallNotPermittedException(CallNotPermittedException ex) {
        logger.warn(ex.getMessage());

        ErrorMessage error = new ErrorMessage("Service unavailable");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler(DependencyException.class)
    protected ResponseEntity<Object> handleDependencyException(DependencyException ex) {
        logger.warn(ex.getMessage());

        ErrorMessage error = new ErrorMessage(String.format(
            "[%s] Unexpected third-party service failure",
            ex.getService()
        ));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }
}