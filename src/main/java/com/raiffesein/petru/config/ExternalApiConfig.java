package com.raiffesein.petru.config;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.Map;

import static io.github.resilience4j.circuitbreaker.CircuitBreakerConfig.SlidingWindowType.COUNT_BASED;

@Configuration
public class ExternalApiConfig {

    @Bean
    public WebClient webClient() {
        return WebClient.builder()
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();
    }

    @Bean
    public CircuitBreakerRegistry circuitBreakerRegistry() {
        CircuitBreakerConfig externalServiceRatesConfig = CircuitBreakerConfig.custom()
            .slidingWindowSize(10)
            .slidingWindowType(COUNT_BASED)
            .waitDurationInOpenState(Duration.ofSeconds(5))
            .minimumNumberOfCalls(5)
            .failureRateThreshold(50.0f)
            .build();
        return CircuitBreakerRegistry.of(
            Map.of("externalServiceRates", externalServiceRatesConfig)
        );
    }
}
