package com.raiffesein.petru.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CachingConfig {

    public static final String EXCHANGE_RATES = "exchangeRates";
    private static final int TIME_TO_LIVE_SECONDS = 1800;

    @Bean
    Config config() {
        Config config = new Config();

        MapConfig mapConfig = new MapConfig();
        mapConfig.setTimeToLiveSeconds(TIME_TO_LIVE_SECONDS);
        config.getMapConfigs().put(EXCHANGE_RATES, mapConfig);

        return config;
    }
}